// --- PLUGINS ---
var gulp = require('gulp');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var googleWebFonts = require('gulp-google-webfonts');
var watch = require('gulp-watch');
var image = require('gulp-image');
var nunjucks = require('gulp-nunjucks');
var htmlbeautify = require('gulp-html-beautify');
var livereload = require('gulp-livereload');



// --- CLEAN ---
gulp.task('clean', function() {
	gulp.src('dist')
		.pipe(clean());
});



// --- COPY ---
gulp.task('copy', function() {
	gulp.src('node_modules/popper.js/dist/**/*')
		.pipe(gulp.dest('dist/popper.js/'));
	gulp.src('node_modules/jquery/dist/**/*')
		.pipe(gulp.dest('dist/jquery/'));
	gulp.src('node_modules/ionicons/dist/**/*')
		.pipe(gulp.dest('dist/ionicons/'));
	gulp.src('node_modules/@fancyapps/fancybox/dist/**/*')
		.pipe(gulp.dest('dist/fancybox/'));
});



// --- FONTS ---
gulp.task('fonts', function () {
	gulp.src('src/fonts/fonts.list')
		.pipe(googleWebFonts())
		.pipe(gulp.dest('dist/fonts'));
});



// --- CSS ---
gulp.task('css', function() {
	gulp.src('src/scss/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass())
		//.pipe(autoprefixer())
		.pipe(cleanCSS({
			rebase: false,
			level: 2
		}))
		.pipe(concat('style.min.css'))
		//.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('dist/css/'));
});



// --- JS ---
var sourcejs = require('./src/js/main.json');
gulp.task('js', function() {
	gulp.src(sourcejs)
		//.pipe(sourcemaps.init())
		.pipe(concat('scripts.min.js'))
		.pipe(uglify())
		//.pipe(sourcemaps.write(''))
		.pipe(gulp.dest('dist/js/'));
});



// --- IMAGES ---
gulp.task('images', function() {
	gulp.src('src/img/**/*')
		.pipe(image())
		.pipe(gulp.dest('dist/img'));
});



// --- FAVICON ---
gulp.task('favicon', function() {
	gulp.src('src/favicon/**/*')
		.pipe(image())
		.pipe(gulp.dest('dist/favicon'));
});



// --- DEFAULT ---
gulp.task('default', function(){
	gulp.start('copy', 'fonts', 'favicon', 'css', 'js', 'images');
});



// --- WATCH ---
gulp.task('watch',function(){
	gulp.watch(['src/scss/**/*'],['css']);
	gulp.watch(['src/js/**/*'],['js']);
	gulp.watch(['src/img/**/*'],['images']);
	
	// LIVERELOAD CHROME EXTENTION
	livereload.listen();
	gulp.watch(['dist/css/*.css']).on('change',livereload.changed);
	gulp.watch(['dist/js/*.js']).on('change',livereload.changed);
});