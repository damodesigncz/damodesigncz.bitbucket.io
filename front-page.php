<?php get_header() ?>
	<div class="container">
		<div class="row">
			<div class="col-xl-10 offset-xl-1">
				<h1 class="my-5 text-center">
					<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/dist/img/logo/logo-white.svg" alt="<?php bloginfo('name'); ?>" width="1920">
				</h1>
				<nav>
					<ul class="nav justify-content-center text-uppercase text-center flex-column flex-md-row my-5 h4">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'home-menu',
									'container' =>'',
									'walker'  => new bs4Navwalker(),
									'fallback_cb' => false,
									'items_wrap' => '%3$s',
								)
							)
						?>
					</ul>
				</nav>
			</div>
		</div>
	</div>
<?php get_footer() ?>