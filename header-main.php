<div class="navbar-light bg-white py-2">
	<div class="container">
		<div class="row">
			<div class="col-8 offset-2 text-center my-1">
				<a class="text-muted d-block" href="<?php bloginfo('url'); ?>">
					<img class="img-fluid" src="<?php bloginfo('template_url'); ?>/dist/img/logo/logo.svg" alt="<?php bloginfo('name'); ?>" width="200">
				</a>
			</div>
			<div class="col-2 d-flex">
				<div class="btn-group my-2 ml-auto">
					<button type="button" class="navbar-toggler px-0 border-0" data-toggle="collapse" data-target="#mainmenu"  aria-expanded="false">
						<span class="navbar-toggler-icon"></span>
						<span class="sr-only">
							<?php _ex('Menu','Toggle Button','theme'); ?>
						</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="collapse" id="mainmenu">
	<div class="navbar-dark bg-dark py-3">
		<div class="container">
			<ul class="nav nav-pills justify-content-end flex-column flex-md-row">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'header-menu',
							'container' =>'',
							'walker'  => new bs4Navwalker(),
							'fallback_cb' => false,
							'items_wrap' => '%3$s',
						)
					)
				?>
			</ul>
		</div>
	</div>
</div>



<header class="py-4">
	<div class="container">
		<div class="h1 d-flex justify-content-center text-primary text-uppercase text-center align-items-center">
			<span>&mdash;</span>
			<<?php if(is_singular('products')) { echo 'h2'; } else { echo 'h1'; } ?> class="h1 m-0 mx-2">
				<?php
					if(is_singular('products') or is_archive('products')) {
						echo get_post_type_object('products')->labels->name;
						if(is_tax()) {
							echo ' / ';
							single_term_title();
						}
					} else {
						the_title();
					}
					
				?>
			</<?php if(is_singular('products')) { echo 'h2'; } else { echo 'h1'; } ?>>
			<span>&mdash;</span>
		</div>
		<?php if(function_exists('bcn_display')): ?>
			<nav>
				<ol class="breadcrumb justify-content-center mb-4 small">
					<?php bcn_display(); ?>
				</ol>
			</nav>
		<?php endif; ?>
	</div>
</header>