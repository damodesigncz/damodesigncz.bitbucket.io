<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>






	<hr>
	<h2>Image</h2>
	<?php $image = get_field('image'); ?>
	<?php if (!empty($image)): ?>
		<a href="<?php echo $image['url']; ?>" title="<?php echo $image['caption']; ?>">
			<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" width="<?php echo $image['sizes']['thumbnail-width' ]; ?>" height="<?php echo $image['sizes']['thumbnail-height' ]; ?>">
		</a>
		<br>
		Url: <?php echo $image['url']; ?>
		<br>
		Title: <?php echo $image['title']; ?>
		<br>
		Caption: <?php echo $image['caption']; ?>
		<br>
		Alt: <?php echo $image['alt']; ?>
		<br>
		Description: <?php echo $image['description']; ?>
		<br>
		Thumbnail: <?php echo $image['sizes']['thumbnail']; ?>
		<br>
		Thumbnail-width: <?php echo $image['sizes']['thumbnail-width' ]; ?>px
		<br>
		Thumbnail-height: <?php echo $image['sizes']['thumbnail-height' ]; ?>px
	<?php endif; ?>







	<hr>
	<h2>File</h2>
	<?php $file = get_field('file'); ?>
	<?php if (!empty($file)): ?>
		File type: <?php echo wp_check_filetype($file['url'])['ext']; ?>
		<br>
		Url: <?php echo $file['url']; ?>
		<br>
		Title: <?php echo $file['title']; ?>
		<br>
		Caption: <?php echo $file['caption']; ?>
		<br>
		Alt: <?php echo $file['alt']; ?>
		<br>
		Description: <?php echo $file['description']; ?>
	<?php endif; ?>






	<hr>
	<h2>Gallery</h2>
	<?php 
		$images = get_field('gallery');
		if(!empty($images)):
	?>
	<div class="gallery">
		<?php foreach( $images as $image ): ?>
			<span class="gallery-item">
				<span class="gallery-icon">
					<a href="<?php echo $image['url']; ?>">
						<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>">
					</a>
				</span>
				<span class="gallery-caption"><?php echo $image['caption']; ?></span>
			</span>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>






	<hr>
	<h2>Repeater</h2>
	<?php if(have_rows('repeater')): ?>
		<?php while(have_rows('repeater')): the_row(); ?>
			<p><?php the_sub_field('item'); ?></p>
		<?php endwhile; ?>
	<?php endif; ?>





<?php endwhile; ?>
<?php get_footer(); ?>
