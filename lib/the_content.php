<?php



// valid HTML5 | remove table > border="0"
function table_border($content) {
    return str_replace('<table border="0"', '<table class="table"', $content);
}
add_filter('the_content', 'table_border');
