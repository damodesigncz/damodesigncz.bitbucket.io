<?php


// prevent generate medium_large size
update_option('medium_large_size_w','0');
update_option('medium_large_size_h','0');



// add image size
add_image_size('thumbnail-510x510',510,510,true);



// image sizes name
function custom_sizes_name($sizes) {
	return array_merge( $sizes, array(
		'thumbnail-360x360' => _x('Thumbnail (Large)','','theme'),
	));
}
add_filter('image_size_names_choose','custom_sizes_name');




// remove special characters from uploaded media
function sanitize_media ($filename) {
	return remove_accents($filename);
}
add_filter('sanitize_file_name', 'sanitize_media', 10);