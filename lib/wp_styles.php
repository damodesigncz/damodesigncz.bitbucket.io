<?php

function wp_theme_styles() {
	wp_deregister_style('contact-form-7');

	wp_enqueue_style('fonts',get_bloginfo('template_url').'/dist/fonts/fonts.css',array(),filemtime(get_theme_file_path('/dist/fonts/fonts.css')),'screen');
	wp_enqueue_style('ionicons',get_bloginfo('template_url').'/dist/ionicons/css/ionicons.min.css',array(),filemtime(get_theme_file_path('/dist/ionicons/css/ionicons.min.css')),'screen');
	wp_enqueue_style('theme',get_bloginfo('template_url').'/dist/css/style.min.css',array(),filemtime(get_theme_file_path('/dist/css/style.min.css')),'all');
	wp_enqueue_style('fancybox',get_bloginfo('template_url').'/dist/fancybox/jquery.fancybox.css',array(),filemtime(get_theme_file_path('/dist/fancybox/jquery.fancybox.css')),'all');
}
add_action('wp_enqueue_scripts','wp_theme_styles',100);