<?php
/**!
 * Bootstrap pagination for index and category pages
 */

if ( ! function_exists( 'b4st_pagination' ) ) {
	function b4st_pagination() {



		global $wp_query;

		$big = 999999999; // need an unlikely integer

		$paginate_links = paginate_links(
			array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'mid_size' => 1,
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages,
				'type' => 'list'
			)
		);


		$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class=\"pagination justify-content-center flex-wrap d-print-none\">", $paginate_links );
		$paginate_links = str_replace( "<li", "<li class=\"page-item\"", $paginate_links );
		$paginate_links = str_replace( "prev page-numbers", "prev page-link", $paginate_links );
		$paginate_links = str_replace( "next page-numbers", "next page-link", $paginate_links );
		$paginate_links = str_replace( "'page-numbers'", "\"page-link\"", $paginate_links );
		$paginate_links = str_replace( '<li class="page-item"><a class="prev ', '<li class="page-item d-none d-md-block"><a class="', $paginate_links );
		$paginate_links = str_replace( '<li class="page-item"><a class="next ', '<li class="page-item d-none d-md-block"><a class="', $paginate_links );

		$paginate_links = str_replace( '<li class="page-item"><span class="page-numbers dots">', '<li class="page-item disabled"><span class="page-link">', $paginate_links );
		$paginate_links = str_replace( "<span aria-current='page' class='page-numbers current'>", '<span class="page-link current">', $paginate_links );
		$paginate_links = str_replace( '<li class="page-item"><span class="page-link current">', '<li class="page-item disabled"><span class="page-link bg-light">', $paginate_links );


		//$paginate_links = str_replace( "page-numbers", "page-link", $paginate_links );
		//$paginate_links = str_replace( "<li class='page-item'><span class=\"page-link dots\", "<li class="page-item disabled"><span class="page-link", $paginate_links );
		//$paginate_links = str_replace( "<li class='page-item'><span aria-current='page' class='", "<li class='page-item active'><span class='disabled ", $paginate_links );





		if ($paginate_links) {
			echo $paginate_links;
		}



/*
		global $wp_query;
		$big = 999999999; // This needs to be an unlikely integer
		// For more options and info view the docs for paginate_links()
		// http://codex.wordpress.org/Function_Reference/paginate_links
		$paginate_links = paginate_links( array(
			'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'mid_size' => 1,
			'prev_next' => True,
			'prev_text' => __('<i class="fas fa-angle-left"></i> Newer', 'b4st'),
			'next_text' => __('Older <i class="fas fa-angle-right"></i>', 'b4st'),
			'type' => 'list'
		) );
		$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination justify-content-center'>", $paginate_links );
		$paginate_links = str_replace( "<li>", "<li class='page-item'>", $paginate_links );
		$paginate_links = str_replace( "<li class='page-item'><span aria-current='page' class='page-numbers current'>", "<li class='page-item disabled active'><span class='page-link bg-primary border-primary text-white'>", $paginate_links );
		$paginate_links = str_replace( "<a", "<a class='page-link' ", $paginate_links );

		$paginate_links = str_replace( '<li class=\'page-item\'><span class=" dots"', '<li class=\'page-item\'><span class="page-link"', $paginate_links );
		//$paginate_links = str_replace( "</span>", "</a>", $paginate_links );
		$paginate_links = preg_replace( "/\s*page-numbers/", "", $paginate_links );
		// Display the pagination if more than one page is found
		if ( $paginate_links ) {
			echo $paginate_links;
		}
*/
	}
}