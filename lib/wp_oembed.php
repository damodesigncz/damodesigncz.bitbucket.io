<?php

// wrap embed videos
function wp_oembed_customize($html, $url, $attr, $post_id) {
	return '<div class="embed-responsive embed-responsive-16by9 mb-3">'.$html.'</div>';
}
add_filter('embed_oembed_html','wp_oembed_customize',99,4);