$(window).scroll(function() {
	winTop = $(window).scrollTop();
	winHeight = $(window).height();
	docHeight = $(document).height() - 5;
	if(winTop + winHeight > docHeight) {
		$('body').addClass('page-on-down');
	}
	if(winTop + winHeight < docHeight) {
		$('body').removeClass('page-on-down');
	}
});