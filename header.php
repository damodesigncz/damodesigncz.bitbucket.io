<!doctype html>
<html <?php language_attributes() ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type') ?>;charset=<?php bloginfo('charset') ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php wp_title(' - ',true,'right'); bloginfo('name'); ?></title>

		<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/dist/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/dist/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/dist/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php bloginfo('template_url'); ?>/dist/favicon/site.webmanifest">
		<link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/dist/favicon/safari-pinned-tab.svg" color="#000000">
		<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/dist/favicon/favicon.ico">
		<meta name="msapplication-TileColor" content="#000000">
		<meta name="msapplication-config" content="<?php bloginfo('template_url'); ?>/dist/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<div class="body">