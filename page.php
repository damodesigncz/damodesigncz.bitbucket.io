<?php get_header(); ?>
<?php get_template_part('header','main'); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
<?php endwhile; ?>
<?php get_footer(); ?>
