<?php get_header() ?>
	<h1><?php _e('Error 404! That page can&rsquo;t be found.','theme'); ?></h1>
	<p><?php _e('It looks like nothing was found at this location. Maybe try a search?','theme'); ?></p>
	<?php get_search_form(); ?>
<?php get_footer() ?>
