<?php get_header(); ?>
<h1><?php echo get_post_type_object('cptslug')->labels->name; ?></h1>












Archive link: <?php echo get_post_type_archive_link('cptslug'); ?>
<br>
Name: <?php echo get_post_type_object('cptslug')->labels->name; ?>






<hr>






<?php while ( have_posts() ) : the_post(); ?>

	<h2><?php the_title(); ?></h2>

	<?php if(has_post_thumbnail()): ?>
		<?php the_post_thumbnail('thumbnail'); ?>
	<?php endif; ?>

	<?php if(get_the_excerpt()): ?>
		<p>Limit excerpt 3: <?php echo excerpt(3); ?></p>
		<p><?php echo get_the_excerpt(); ?></p>
	<?php endif; ?>

	<a href="<?php the_permalink(); ?>">Permalink</a>

<?php endwhile; ?>






<hr>




<?php
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	query_posts(array(
		'paged' => $paged,
		'post_type' => 'cptslug',
		'posts_per_page' => 1,
		'orderby' => array('date','title'), // date, title, rand, menu_order
		'order'    => 'ASC', // ASC, DESC
	));
?>
<?php if(have_posts()): ?>

	<?php while(have_posts()) : the_post(); ?>
		<h2><?php the_title(); ?></h2>
	<?php endwhile; ?>

	<?php if(has_next_posts or has_previous_posts): ?>
		<?php posts_nav_link(' | ','<span>'.__('Newer posts','theme').'</span>','<span>'.__('Older posts','theme').'</span>'); ?>
	<?php endif; ?>
	
<?php endif; ?>
<?php wp_reset_query(); ?>








<?php get_footer(); ?>