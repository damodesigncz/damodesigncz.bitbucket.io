<?php


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> __('Contact Info'),
		'menu_title'	=> __('Contact Info'),
		'menu_slug' 	=> 'contact-settings',
		'capability'	=> 'edit_posts',
		'icon_url'      => 'dashicons-id',
		'redirect'		=> false
	));
}



if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-option-contacts',
		'title' => __('Contact Info'),
		'label_placement' => 'left',
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'contact-settings',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'option-email',
				'name' => 'option-email',
				'label' => _x('Email','ACF field','theme'),
				'type' => 'email',
			),
			array (
				'key' => 'option-phone',
				'name' => 'option-phone',
				'label' => _x('Phone','ACF field','theme'),
				'type' => 'text',
			)
		),
	));
}