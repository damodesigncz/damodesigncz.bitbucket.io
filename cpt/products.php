<?php

function register_cpt_products() {
	$labels = array(
		'name'                => _x( 'Products', 'Post Type General Name', 'theme' ),
		'singular_name'       => _x( 'Products', 'Post Type Singular Name', 'theme' ),
		'menu_name'           => _x( 'Products', 'Post Type General Name', 'theme' ),
		'name_admin_bar'      => _x( 'Products', 'Post Type General Name','add new from admin bar').' - '.strtolower(__( 'New Post')),
		'all_items'           => __( 'All Posts' ), // or 'All Pages'
		'add_new'             => _x( 'Add New', 'post'), // or 'Add New', 'page'
		'add_new_item'        => __( 'Add New Post'), // or 'Add New Page'
		'edit_item'           => __( 'Edit Post'), // or 'Edit Page'
		'new_item'            => __( 'New Post'), // or 'New Page'
		'view_item'           => __( 'View Post'), // or 'View Page'
		'search_items'        => __( 'Search Posts'), // or 'Search Pages'
		'not_found'           => __( 'No posts found.'), // or 'No pages found.'
		'not_found_in_trash'  => __( 'No posts found in Trash.'), // or 'No pages found in Trash.'
	);
	$rewrite = array(
	    'slug'                => _x( 'products', 'Post Type Slug', 'theme' ),
	);
	$args = array(
		'supports'            => array( 'title','editor' ),
		'menu_icon'           => 'dashicons-cart',
		'menu_position'       => 5,
		'labels'              => $labels,
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page'
	);
	register_post_type( 'products', $args );
}
add_action( 'init', 'register_cpt_products', 0 );


// admin order
if (is_admin()) {
	function cpt_products_order($wp_query) {
		$post_type = $wp_query->query['post_type'];
		if ( $post_type == 'products') {
			$wp_query->set('orderby', 'date');
			$wp_query->set('order', 'DESC');
		}
	}
	add_filter('pre_get_posts', 'cpt_products_order');
}


// register taxonomy
function create_product_category_taxonomies() {
	$labels = array(
		'name'              => _x( 'Product Category', 'taxonomy general name', 'theme' ),
		'singular_name'     => _x( 'Product Category', 'taxonomy singular name', 'theme' ),
		'menu_name'         => __( 'Product Category', 'theme' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'product-category', 'hierarchical' => true ),
	);
	register_taxonomy( 'product-category', 'products', $args );
}
add_action( 'init', 'create_product_category_taxonomies', 0 );




// ACF
if(function_exists('acf_add_local_field_group')) {
	acf_add_local_field_group(array(
		'key' => 'meta-contacts',
		'title' => _x('Gallery','ACF field','theme'),
		'position' => 'acf_after_title',
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'products',
				),
			),
		),
		'fields' => array (
			array (
				'key' => 'product_gallery',
				'name' => 'product_gallery',
				'label' => _x('Assign images to gallery','ACF field','theme'),
				'type' => 'gallery',
				'required' => true,
			),
		),
	));
}



// admin css
function products_admin_css() {
	echo '<style>
		#newproduct-category_parent,
		.post-type-products #postexcerpt .inside p {
			display: none;
		}
	</style>';
}
add_action('admin_head', 'products_admin_css');