<?php get_header(); ?>
<?php get_template_part('header','main'); ?>
<main>
	<div class="py-3">
		<div class="container">
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				if(is_tax()){
					$term = get_term_by('slug',get_query_var('term'),get_query_var('taxonomy')); 
					query_posts(array(
						'paged' => $paged,
						'post_type' => 'products',
						'orderby' => 'date',
						'order' => 'DESC',
						'tax_query' => array(
							array(
								'taxonomy' => 'product-category',
								'field' => 'term_id',
								'terms' => $term->term_id,
							),
						),
					));
				} else {
					query_posts(array(
						'paged' => $paged,
						'post_type' => 'products',
						'orderby' => 'date',
						'order' => 'DESC',
					));
				}
			?>
			<?php if(have_posts()): ?>
				<div class="row justify-content-center">
					<?php while(have_posts()) : the_post(); ?>
						<div class="col-9 col-xs-6 col-lg-4 mb-4">
							<a class="d-block" href="<?php the_permalink(); ?>">
								<figure class="figure">
									<span class="hover-zoom">
										<?php $images = get_field('product_gallery'); ?>
										<?php if($images): ?>
											<?php $i=0; foreach( $images as $image ): $i++; ?>
												<?php echo wp_get_attachment_image($image['ID'],'thumbnail-360x360','',array('class'=>'img-fluid border')); ?>
												<?php if($i==1) break; ?>
											<?php endforeach; ?>
										<?php endif; ?>

									</span>
									<figcaption class="figure-caption text-center mt-1">
										<span class="h5">
											<?php the_title(); ?>
										</span>
									</figcaption>
								</figure>
							</a>
						</div>
					<?php endwhile; ?>
				</div>

				<?php if(has_next_posts or has_previous_posts): ?>
					<?php posts_nav_link(' | ','<span>'.__('Newer posts','theme').'</span>','<span>'.__('Older posts','theme').'</span>'); ?>
				<?php endif; ?>

			<?php else: ?>

				<p class="text-center lead"><?php _ex('No products found.','List products empty','theme'); ?></p>
				
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>
