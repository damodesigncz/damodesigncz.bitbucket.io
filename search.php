<?php get_header(); ?>
	<h1><?php _e('Search','theme'); ?></h1>
	<?php get_search_form(); ?>
	<?php if(!empty($_GET['s'])) : ?>
		<h2><?php printf(__( 'Search Results for: %s','theme'), get_search_query() ); ?></h2>
		<?php
			while ( have_posts() ) : the_post(); ?>
			<h2>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<small>- <?php echo get_post_type_object(get_post_type(get_the_ID()))->labels->name; ?></small>
			</h2>
			<?php the_excerpt(); ?>
		<?php endwhile; ?>
		<?php if(has_next_posts or has_previous_posts): ?>
			<nav class="pager">
				<?php posts_nav_link(' ','<span>&larr; '.__('Previous search results','theme').'</span>','<span>'.__('Next search results','theme').' &rarr;</span>'); ?>
			</nav>
		<?php endif; ?>
	<?php endif; ?>
<?php get_footer(); ?>