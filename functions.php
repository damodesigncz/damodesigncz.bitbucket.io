<?php

// Custom Post Types
require('cpt/products.php');

// libs
require('lib/languages.php');
require('lib/the_content.php');
require('lib/images.php');
require('lib/gallery.php');
require('lib/remove_post_types.php');
require('lib/wp_head.php');
require('lib/nav_menu.php');
require('lib/wp_scripts.php');
require('lib/wp_styles.php');
require('lib/bs4_navwalker.php');
require('lib/bs4_pagination.php');
require('lib/wp_oembed.php');
require('lib/yoast_seo.php');

// Advanced custom fields
require('acf/theme_options.php');