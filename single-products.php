<?php get_header(); ?>
<?php get_template_part('header','main'); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<?php $images = get_field('product_gallery'); ?>
				<?php if($images): ?>
					<div class="row justify-content-center">
						<?php $i=0; foreach( $images as $image ): $i++; ?>
							<div class="<?php if($i==1) { echo 'col-sm-12'; } else { echo 'col-4'; } ?> mb-4">
								<a data-fancybox="gallery" href="<?php echo $image['url']; ?>" class="hover-zoom">
									<?php
										if($i==1) {
											$class = 'img-fluid border';
											$size = 'thumbnail-510x510';
										} else {
											$class = 'img-fluid border';
											$size = 'thumbnail';
										}
										echo wp_get_attachment_image($image['ID'],$size,'',array('class'=>$class));
									?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-7">
				<h1><?php the_title(); ?></h1>
				<p>
					<button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#order">
						<?php _ex('Interested in','Toggle Button','theme'); ?>
					</button>
					<?php
						echo get_the_term_list($post->ID,'product-category',__('Categories').': ', ', ');
					?>
				</p>
				<?php the_content(); ?>
			</div>
		</div>
	</div>



	<!-- Modal -->
	<div class="modal fade align-items-center" id="order" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">
						<?php the_title(); ?>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php echo do_shortcode('[contact-form-7 id="66" title="Objednávka"]'); ?>
				</div>
			</div>
		</div>
	</div>

<?php endwhile; ?>
<?php get_footer(); ?>
